import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class InputReader {

    public void init(KeyboardHandler game) {

        Keyboard keyboard = new Keyboard(game);

        KeyboardEvent upRightside = new KeyboardEvent();
        upRightside.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        upRightside.setKey(KeyboardEvent.KEY_I);
        keyboard.addEventListener(upRightside);

        KeyboardEvent upRightsideRelease = new KeyboardEvent();
        upRightsideRelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        upRightsideRelease.setKey(KeyboardEvent.KEY_I);
        keyboard.addEventListener(upRightsideRelease);

        KeyboardEvent leftRightside = new KeyboardEvent();
        leftRightside.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        leftRightside.setKey(KeyboardEvent.KEY_J);
        keyboard.addEventListener(leftRightside);

        KeyboardEvent leftRightsideRelease = new KeyboardEvent();
        leftRightsideRelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        leftRightsideRelease.setKey(KeyboardEvent.KEY_J);
        keyboard.addEventListener(leftRightsideRelease);

        KeyboardEvent rightRightside = new KeyboardEvent();
        rightRightside.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        rightRightside.setKey(KeyboardEvent.KEY_L);
        keyboard.addEventListener(rightRightside);

        KeyboardEvent rightRightsideRelease = new KeyboardEvent();
        rightRightsideRelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        rightRightsideRelease.setKey(KeyboardEvent.KEY_L);
        keyboard.addEventListener(rightRightsideRelease);


        KeyboardEvent leftLeftside = new KeyboardEvent();
        leftLeftside.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        leftLeftside.setKey(KeyboardEvent.KEY_A);
        keyboard.addEventListener(leftLeftside);

        KeyboardEvent leftLeftsiderelease = new KeyboardEvent();
        leftLeftsiderelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        leftLeftsiderelease.setKey(KeyboardEvent.KEY_A);
        keyboard.addEventListener(leftLeftsiderelease);

        KeyboardEvent rightLeftside = new KeyboardEvent();
        rightLeftside.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        rightLeftside.setKey(KeyboardEvent.KEY_D);
        keyboard.addEventListener(rightLeftside);

        KeyboardEvent rightLeftsideRelease = new KeyboardEvent();
        rightLeftsideRelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        rightLeftsideRelease.setKey(KeyboardEvent.KEY_D);
        keyboard.addEventListener(rightLeftsideRelease);


        KeyboardEvent upLeftside = new KeyboardEvent();
        upLeftside.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        upLeftside.setKey(KeyboardEvent.KEY_W);
        keyboard.addEventListener(upLeftside);

        KeyboardEvent upLeftsideRelease = new KeyboardEvent();
        upLeftsideRelease.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        upLeftsideRelease.setKey(KeyboardEvent.KEY_W);
        keyboard.addEventListener(upLeftsideRelease);

        KeyboardEvent Yes = new KeyboardEvent();
        Yes.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        Yes.setKey(KeyboardEvent.KEY_Y);
        keyboard.addEventListener(Yes);

        KeyboardEvent No = new KeyboardEvent();
        No.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        No.setKey(KeyboardEvent.KEY_N);
        keyboard.addEventListener(No);

        KeyboardEvent start = new KeyboardEvent();
        start.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        start.setKey(KeyboardEvent.KEY_SPACE);
        keyboard.addEventListener(start);

        KeyboardEvent exit = new KeyboardEvent();
        exit.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        exit.setKey(KeyboardEvent.KEY_E);
        keyboard.addEventListener(exit);
    }
}
