import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class StartScreen {

    private Rectangle startScreen;
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 500;
    public static final int PADDING = 10;

    public StartScreen() {
        startScreen = new Rectangle(PADDING, PADDING, WIDTH, HEIGHT);
        startScreen.setColor(Color.BLACK);
        startScreen.fill();
    }

    public void hideStartScreen(){
        startScreen.delete();
    }

}
