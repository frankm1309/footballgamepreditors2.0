import java.io.IOException;

public class Main  {
    public static void main(String[] args) throws InterruptedException{

        Game g = new Game();
        g.init();
        g.start();
    }
}
