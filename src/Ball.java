import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Ball implements Collidable {

    public static final float SPEED_DECREMENT = .001f;

    private float speedX, speedY;
    private Picture ball;
    private int posX, posY;
    private String ballPicture;

    public Ball(int posX, int posY, String ballPicture) {
        this.posX = posX;
        this.posY = posY;
        this.ballPicture = ballPicture;
        ball = new Picture(posX, posY, ballPicture);
        ball.draw();
        speedX = 0;
        speedY = 0;
    }

    @Override
    public int getX() {
        return ball.getX();
    }

    @Override
    public int getY() {
        return ball.getY();
    }

    @Override
    public int getMaxX() {
        return ball.getMaxX();
    }

    @Override
    public int getMaxY() {
        return ball.getMaxY();
    }

    public void reset() {
        ball.delete();
        speedX = 0;
        speedY = 0;
        ball = new Picture(posX, posY, ballPicture);
        ball.draw();
    }

    public void move() {
        float gravity = .995f;
        speedY = speedY * gravity;
        speedY = speedY + 0.05f * gravity;

        if (ball.getX() + speedX >= Field.WIDTH + Field.PADDING - ball.getWidth() || ball.getX() <= Field.PADDING) {
            speedX =- speedX;
        }

        if (ball.getY() + speedY >= Field.HEIGHT + Field.PADDING - ball.getHeight() || ball.getY() <= Field.PADDING + 2) {
            speedY =- speedY;
        }

        ball.translate(speedX, speedY);
        ball.draw();

        if (speedX < 0) {
            speedX += SPEED_DECREMENT;
        } else {
            speedX -= SPEED_DECREMENT;
        }
    }

    public void setHitSpeed(int speedX, int speedY) {
        this.speedX = speedX;
        this.speedY = speedY;
    }

    @Override
    public boolean isCollidingWith(Collidable collidable) {

        // If one rectangle is on left side of other
        if (ball.getX() > collidable.getMaxX() || collidable.getX() > ball.getMaxX())
            return false;

        // If one rectangle is above other
        return ball.getY() <= collidable.getMaxY() && collidable.getY() <= ball.getMaxY();

    }

    public boolean isCollidingWithTopOfGoal1(Goal goal) {
        return ball.getMaxY() + Field.PADDING >= goal.getY() && ball.getX() < goal.getMaxX();
    }

    public boolean isCollidingWithTopOfGoal2(Goal goal) {
        return ball.getMaxY() + Field.PADDING >= goal.getY() && ball.getMaxX() > goal.getX();
    }

}
