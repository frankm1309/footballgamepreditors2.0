import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;


public class Game implements KeyboardHandler {

    private static final int WINNING_SCORE = 10;
    private Sound fxPlayer;
    private Ball ball;
    private Player player1, player2;
    private Goal goal1, goal2;
    private boolean gameOver = false;
    private int scoreP1 = 0;
    private int scoreP2 = 0;
    private Text textP1;
    private Text textP2;
    private Text textWinning;
    private Text playAgainText;
    private boolean startScreen = true;
    private StartScreen intro;

    public void init() throws InterruptedException {
        intro = new StartScreen();
        InputReader kb = new InputReader();
        kb.init(this);

        Sound startSound = new Sound("resources/sound/intro.au");
        startSound.play(true);

        Picture logo = new Picture((Field.WIDTH / 2) - 280, 60, "resources/img/pokemonHeadSoccerLogo.png");
        logo.draw();
        Text options = new Text((Field.WIDTH / 2) - 60, 420, "Press SPACEBAR to play, E to exit.");
        options.grow(150, 15);

        while (startScreen) {
            options.setColor(Color.WHITE);
            options.draw();
            Thread.sleep(800);
            options.delete();
            Thread.sleep(800);
        }

        logo.delete();
        intro.hideStartScreen();
        startSound.close();

        new Field();
        player1 = new Player(200, 460, "resources/img/charmander.png", 1);
        player2 = new Player(700, 460, "resources/img/squirtle.png", 2);
        ball = new Ball((Field.WIDTH / 2), 20, "resources/img/pokeball.png");
        goal1 = new Goal(Field.PADDING, Field.HEIGHT - 190, "resources/img/newGoal-p1.png");
        goal2 = new Goal(Field.WIDTH - 90, Field.HEIGHT - 190, "resources/img/newGoal-p2.png");

        textP1 = new Text(Field.WIDTH / 2 - 100, 30, "P" + player1.getPlayerNumber() + ": " + scoreP1);
        textP1.grow(45, 20);
        textP1.setColor(Color.WHITE);
        textP1.draw();

        textP2 = new Text(Field.WIDTH / 2 + 100, 30, "P"+ player2.getPlayerNumber() + ": " + scoreP2);
        textP2.grow(45, 20);
        textP2.setColor(Color.WHITE);
        textP2.draw();

        fxPlayer = new Sound("resources/sound/jump.au");

        Sound battleSound = new Sound("resources/sound/pokemonBattle.au");
        battleSound.play(true);

    }

    public void start() throws InterruptedException {

        while (!gameOver) {

            Thread.sleep(10);
            ball.move();
            player1.move();
            player2.move();

            int randomSteps = 10;
            if (ball.isCollidingWith(player1)) {
                ball.setHitSpeed((int) (Math.random() * randomSteps), (int) (Math.random() * randomSteps));
            }

            if (ball.isCollidingWith(player2)) {
                ball.setHitSpeed((int) (Math.random() * -randomSteps), (int) (Math.random() * -randomSteps));
            }

            if (ball.isCollidingWithTopOfGoal1(goal1)){
                ball.setHitSpeed((int) (Math.random() * randomSteps), (int) (Math.random() * randomSteps));
            }

            if (ball.isCollidingWithTopOfGoal2(goal2)){
                ball.setHitSpeed((int) (-Math.random() * randomSteps), (int) (Math.random() * randomSteps));
            }

            if (player1.isCollidingWith(player2)) {
                player1.stepBack(-1);
                player2.stepBack(1);
            }

            if (player1.isCollidingWith(goal1)) {
                player1.stepBack(1);
            }

            if (player1.isCollidingWith(goal2)) {
                player1.stepBack(-1);
            }

            if (player2.isCollidingWith(goal1)) {
                player2.stepBack(1);
            }

            if (player2.isCollidingWith(goal2)) {
                player2.stepBack(-1);
            }

            if (goal1.isHit1(ball)) {
                scoreP2++;
                updateScoreText(textP2, scoreP2, player2.getPlayerNumber());
                ball.reset();
            }

            if (goal2.isHit2(ball)) {
                scoreP1++;
                updateScoreText(textP1, scoreP1, player1.getPlayerNumber());
                ball.reset();
            }

            if (scoreP1 == WINNING_SCORE) {
                setWinningText(player1);
            }

            if (scoreP2 == WINNING_SCORE) {
                setWinningText(player2);
            }
        }

        while (gameOver) {
            Thread.sleep(1);
        }
        start();
    }

    public void setWinningText(Player player){
        makeWinningMessage(player.getPlayerNumber());
        turnOnMessage(textWinning, true);
        gameOver = true;
        makePlayAgainText();
        turnOnMessage(playAgainText, true);
    }

    public void resetValues() {
        scoreP2 = 0;
        scoreP1 = 0;
        updateScoreText(textP1, scoreP1, player1.getPlayerNumber());
        updateScoreText(textP2, scoreP2, player2.getPlayerNumber());
        player1.setPlayerOnPos(200, 460);
        player2.setPlayerOnPos(700, 460);
        turnOnMessage(textWinning, false);
        turnOnMessage(playAgainText, false);
    }

    private void makeGoodbyeMessage() {
        Text textGoodbye = new Text(Field.WIDTH / 2, Field.HEIGHT / 2, "OOOHHHH NOOOOOO");
        textGoodbye.grow(75, 50);
        textGoodbye.setColor(Color.WHITE);
        textGoodbye.draw();
    }

    private void makeWinningMessage(int playerNumber) {
        textWinning = new Text(Field.WIDTH / 2, Field.HEIGHT / 2, "PLAYER " + playerNumber + " WINS");
        textWinning.grow(75, 50);
        textWinning.setColor(Color.WHITE);
    }

    private void turnOnMessage(Text text, boolean on) {
        if (on) {
            text.draw();
        } else {
            text.delete();
        }
    }

    private void makePlayAgainText() {
        playAgainText = new Text(Field.WIDTH / 2 - 30, Field.HEIGHT / 2 + 100, "PLAY AGAIN? YES/NO");
        playAgainText.grow(75, 50);
        playAgainText.setColor(Color.WHITE);
    }

    private void updateScoreText(Text text, int score, int playerNumber) {
        text.delete();
        text.setText("P"+playerNumber + ": " + score);
        text.draw();
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        int speed = 5;
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_D:
                player1.setSpeedX(speed);
                break;
            case KeyboardEvent.KEY_A:
                player1.setSpeedX(-speed);
                break;
            case KeyboardEvent.KEY_W: {
                if (player1.getMaxY() >= (Field.HEIGHT + Field.PADDING - 20)) {
                    player1.setSpeedY(-speed);
                    fxPlayer.play(true);
                }
                break;
            }
            case KeyboardEvent.KEY_L:
                player2.setSpeedX(speed);
                break;
            case KeyboardEvent.KEY_J:
                player2.setSpeedX(-speed);
                break;
            case KeyboardEvent.KEY_I:
                if (player2.getMaxY() >= (Field.HEIGHT + Field.PADDING - 20)) {
                    player2.setSpeedY(-speed);
                    fxPlayer.play(true);
                }
                break;
            case KeyboardEvent.KEY_Y:
                if (gameOver) {
                    resetValues();
                    gameOver = false;
                }
                break;
            case KeyboardEvent.KEY_N:
                if (gameOver) {
                    makeGoodbyeMessage();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.exit(0);
                }
                break;
            case KeyboardEvent.KEY_E:
                if (startScreen) {
                    System.exit(0);
                }
                break;
            case KeyboardEvent.KEY_SPACE:
                if (startScreen) {
                    startScreen = false;
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_D:
            case KeyboardEvent.KEY_A:
                player1.setSpeedX(0);
                break;
            case KeyboardEvent.KEY_W:
                if (player1.getMaxY() < (Field.HEIGHT + Field.PADDING - 20)) {
                    player1.setGravity();
                }
                break;
            case KeyboardEvent.KEY_L:
            case KeyboardEvent.KEY_J:
                player2.setSpeedX(0);
                break;
            case KeyboardEvent.KEY_I:
                if (player2.getMaxY() < (Field.HEIGHT + Field.PADDING - 20)) {
                    player2.setGravity();
                }
                break;
        }
    }
}
