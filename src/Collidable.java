public interface Collidable {

    int getX();
    int getY();
    int getMaxX();
    int getMaxY();

    boolean isCollidingWith(Collidable collidable);


}
