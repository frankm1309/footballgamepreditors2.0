import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Field{

    public static final int WIDTH = 1000;
    public static final int HEIGHT = 500;
    public static final int PADDING = 10;
    public static final int X = 0;


    public Field() {
        Rectangle field = new Rectangle(10, 10, WIDTH, HEIGHT);
        field.setColor(Color.GRAY);
        field.fill();
        Picture clField = new Picture(10, 10, "resources/img/BG2-pixelated.png");
        clField.draw();
    }


}
