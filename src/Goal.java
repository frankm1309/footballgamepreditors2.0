import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Goal implements Collidable {

    private Picture goal;

    public Goal(int posX, int posY, String goalPicture) {
        goal = new Picture(posX, posY, goalPicture);
        goal.draw();
    }

    @Override
    public int getX() {
        return goal.getX();
    }

    @Override
    public int getY() {
        return goal.getY();
    }

    @Override
    public int getMaxY() {
        return goal.getMaxY();
    }

    @Override
    public boolean isCollidingWith(Collidable collidable) {
        return false;
    }

    @Override
    public int getMaxX() {
        return goal.getMaxX();
    }

    public boolean isHit1(Ball ball) {
        return (ball.getX() <= goal.getWidth() + goal.getX() && ball.getY() >= goal.getY());
    }

    public boolean isHit2(Ball ball) {
        return (ball.getMaxX() >= goal.getX() && ball.getY() >= goal.getY());
    }
}
