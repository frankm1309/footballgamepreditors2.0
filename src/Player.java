import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player implements Collidable {

    private float speedX;
    private float speedY;
    private Picture player;
    private float gravity = .995f;
    private String playerPicture;
    private int playerNumber;


    public Player(int posX, int posY, String playerPicture, int playerNumber) {
        this.playerPicture = playerPicture;
        player = new Picture(posX, posY, playerPicture);
        this.playerNumber = playerNumber;
        player.draw();
    }

    public void setPlayerOnPos(int posX, int posY) {
        player.delete();
        player = new Picture(posX, posY, playerPicture);
        player.draw();
    }

    public void stepBack(double stepsX) {
        setSpeedX(0);
        player.translate(stepsX, 0);
    }

    @Override
    public int getX() {
        return player.getX();
    }

    @Override
    public int getY() {
        return player.getY();
    }

    @Override
    public int getMaxX() {
        return player.getX() + player.getWidth();
    }

    @Override
    public int getMaxY() {
        return player.getY() + player.getHeight();
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setSpeedY(int speedY) {
        this.speedY = speedY;
    }

    public void setGravity() {
        speedY = speedY * gravity;
        speedY = speedY + 0.20f * gravity;
    }

    public void move() {

        if (getMaxY() < Field.HEIGHT + Field.PADDING - 20) {
            speedY = speedY * gravity;
            speedY = speedY + 0.08f * gravity;
        }
        if (getX() <= (Field.X + Field.PADDING)) {
            setSpeedX(0);
            player.translate(0.5, 0);
            return;
        }
        if (getMaxX() >= (Field.WIDTH + Field.PADDING)) {
            setSpeedX(0);
            player.translate(-0.5, 0);
            return;
        }
        if (getMaxY() >= Field.HEIGHT + Field.PADDING) {
            setSpeedY(0);
            player.translate(0, -0.5);
        }

        player.translate(speedX, speedY);
        player.draw();
    }

    @Override
    public boolean isCollidingWith(Collidable collidable) {
        // If one rectangle is on left side of other
        if (player.getX() > collidable.getMaxX() || collidable.getX() > player.getX() + player.getWidth()) {
            return false;
        }
        // If one rectangle is above other
        if (player.getY() > collidable.getMaxY() || collidable.getY() > player.getY() + player.getHeight()) {
            return false;
        }

        return true;
    }

}
